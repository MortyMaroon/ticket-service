package com.epam.ticketservice.controllers;

import com.epam.ticketservice.dto.FlightDTO;
import com.epam.ticketservice.services.interfaces.FlightService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/flight")
public class FlightController {
    private final FlightService flightService;

    public FlightController(FlightService flightService) {
        this.flightService = flightService;
    }

    @GetMapping
    public ResponseEntity<List<FlightDTO>> findAll() {
        return ResponseEntity.ok(flightService.findAll().stream().
                map(flightService::collectDTO).
                collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<FlightDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok(flightService.collectDTO(flightService.findById(id)));
    }

    @PostMapping
    public ResponseEntity<FlightDTO> save(@RequestBody FlightDTO flight) {
        return ResponseEntity.ok(flightService.collectDTO(flightService.save(flight)));
    }

    @PutMapping
    public ResponseEntity<FlightDTO> update(@RequestBody FlightDTO user) {
        return ResponseEntity.ok(flightService.collectDTO(flightService.update(user)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Long id) {
        flightService.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
