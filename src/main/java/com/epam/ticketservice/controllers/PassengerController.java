package com.epam.ticketservice.controllers;

import com.epam.ticketservice.dto.PassengerDTO;
import com.epam.ticketservice.services.interfaces.PassengerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/passenger")
public class PassengerController {
    private final PassengerService passengerService;

    public PassengerController(PassengerService passengerService) {
        this.passengerService = passengerService;
    }

    @GetMapping
    public ResponseEntity<List<PassengerDTO>> findAll() {
        return ResponseEntity.ok(passengerService.findAll().stream().
                map(passengerService::collectDTO).
                collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<PassengerDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok(passengerService.collectDTO(passengerService.findById(id)));
    }

    @PostMapping
    public ResponseEntity<PassengerDTO> save(@RequestBody PassengerDTO passenger) {
        return ResponseEntity.ok(passengerService.collectDTO(passengerService.save(passenger)));
    }

    @PutMapping
    public ResponseEntity<PassengerDTO> update(@RequestBody PassengerDTO passenger) {
        return ResponseEntity.ok(passengerService.collectDTO(passengerService.update(passenger)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Long id) {
        passengerService.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
