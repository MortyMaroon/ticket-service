package com.epam.ticketservice.controllers;

import com.epam.ticketservice.dto.TicketDTO;
import com.epam.ticketservice.services.interfaces.TicketService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/ticket")
public class TicketController {
    private final TicketService ticketService;

    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @GetMapping
    public ResponseEntity<List<TicketDTO>> findAll() {
        return ResponseEntity.ok(ticketService.findAll().stream().
                map(ticketService::collectDTO).
                collect(Collectors.toList()));
    }

    @GetMapping("/passenger/{id}")
    public ResponseEntity<List<TicketDTO>> findByPassengerID(@PathVariable Long id) {
        return ResponseEntity.ok(ticketService.findByPassengerId(id).stream().
                map(ticketService::collectDTO).
                collect(Collectors.toList()));
    }

    @GetMapping("/flight/{id}")
    public ResponseEntity<List<TicketDTO>> findByFlightID(@PathVariable Long id) {
        return ResponseEntity.ok(ticketService.findByFlightId(id).stream().
                map(ticketService::collectDTO).
                collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<TicketDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok(ticketService.collectDTO(ticketService.findById(id)));
    }

    @PutMapping
    public ResponseEntity<TicketDTO> update(@RequestBody TicketDTO ticket) {
        return ResponseEntity.ok(ticketService.collectDTO(ticketService.update(ticket)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Long id) {
        ticketService.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
