package com.epam.ticketservice.dao;

import com.epam.ticketservice.dao.util.SessionCreator;
import com.epam.ticketservice.entities.Flight;
import org.hibernate.Session;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
public class FlightDAO {
    private final SessionCreator sessionCreator;

    public FlightDAO(SessionCreator sessionCreator) {
        this.sessionCreator = sessionCreator;
    }

    public List<Flight> findAllWithoutDeleted() {
        Session session = sessionCreator.beginTransaction();
        List<Flight> flights = session.createQuery("select f from Flight f where f.isDeleted = false").getResultList();
        session.getTransaction().commit();
        return flights;

    }

    public List<Flight> findAll() {
        Session session = sessionCreator.beginTransaction();
        List<Flight> flights = session.createQuery("from Flight").getResultList();
        session.getTransaction().commit();
        return flights;
    }

    public Flight findById(Long id) {
        Session session = sessionCreator.beginTransaction();
        Flight flight = session.get(Flight.class,id);
        session.getTransaction().commit();
        return flight;
    }

    public Flight save(Flight flight) {
        Session session = sessionCreator.beginTransaction();
        session.save(flight);
        session.getTransaction().commit();
        return flight;
    }

    public Flight update(Flight flight) {
        Session session = sessionCreator.beginTransaction();
        session.update(flight);
        session.getTransaction().commit();
        return flight;
    }

    public void deleteById(Long id) {
        Session session = sessionCreator.beginTransaction();
        session.get(Flight.class, id).
                setDeleted(true);
        session.getTransaction().commit();
    }
}
