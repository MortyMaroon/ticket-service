package com.epam.ticketservice.dao;

import com.epam.ticketservice.dao.util.SessionCreator;
import com.epam.ticketservice.entities.Passenger;
import org.hibernate.Session;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
public class PassengerDAO {
    private final SessionCreator sessionCreator;

    public PassengerDAO(SessionCreator sessionCreator) {
        this.sessionCreator = sessionCreator;
    }

    public List<Passenger> findAllWithoutDeleted() {
        Session session = sessionCreator.beginTransaction();
        List<Passenger> passengers = session.createQuery("select p from Passenger p where p.isDeleted = false").getResultList();
        session.getTransaction().commit();
        return passengers;
    }

    public List<Passenger> findAll() {
        Session session = sessionCreator.beginTransaction();
        List<Passenger> passengers = session.createQuery("from Passenger").getResultList();
        session.getTransaction().commit();
        return passengers;
    }

    public Passenger findById(Long id) {
        Session session = sessionCreator.beginTransaction();
        Passenger passenger = session.get(Passenger.class, id);
        session.getTransaction().commit();
        return passenger;
    }

    public Passenger save(Passenger passenger) {
        Session session = sessionCreator.beginTransaction();
        session.save(passenger);
        session.getTransaction().commit();
        return passenger;
    }

    public Passenger update(Passenger passenger) {
        Session session = sessionCreator.beginTransaction();
        session.update(passenger);
        session.getTransaction().commit();
        return passenger;
    }

    public void deleteById(Long id) {
        Session session = sessionCreator.beginTransaction();
        session.get(Passenger.class, id).
                setDeleted(true);
        session.getTransaction().commit();
    }
}
