package com.epam.ticketservice.dao;

import com.epam.ticketservice.dao.util.SessionCreator;
import com.epam.ticketservice.entities.Passenger;
import com.epam.ticketservice.entities.Ticket;
import org.hibernate.Session;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Transactional
public class TicketDAO {
    private final SessionCreator sessionCreator;

    public TicketDAO(SessionCreator sessionCreator) {
        this.sessionCreator = sessionCreator;
    }

    public List<Ticket> findAll() {
        Session session = sessionCreator.beginTransaction();
        List<Ticket> tickets = session.createQuery("from Ticket").getResultList();
        session.getTransaction().commit();
        return tickets;
    }

    public List<Ticket> findAllWithoutDeleted() {
        Session session = sessionCreator.beginTransaction();
        List<Ticket> tickets = session.createQuery("select t from Ticket t where t.isDeleted = false").getResultList();
        session.getTransaction().commit();
        return tickets;
    }

    public List<Ticket> findByPassengerID(Long id) {
        Session session = sessionCreator.beginTransaction();
        List<Ticket> tickets = session.createNamedQuery("Ticket.findByPassengerID", Ticket.class).setParameter("id",id).getResultList();
        session.getTransaction().commit();
        return tickets;
    }

    public List<Ticket> findByFlightId(Long id) {
        Session session = sessionCreator.beginTransaction();
        List<Ticket> tickets = session.createNamedQuery("Ticket.findByFlightID", Ticket.class).setParameter("id",id).getResultList();
        session.getTransaction().commit();
        return tickets;
    }

    public Ticket findById(Long id) {
        Session session = sessionCreator.beginTransaction();
        Ticket ticket = session.get(Ticket.class, id);
        session.getTransaction().commit();
        return ticket;
    }

    public Ticket save(Ticket ticket) {
        Session session = sessionCreator.beginTransaction();
        session.save(ticket);
        session.getTransaction().commit();
        return ticket;
    }

    public Ticket update(Ticket ticket) {
        Session session = sessionCreator.beginTransaction();
        session.update(ticket);
        session.getTransaction().commit();
        return ticket;
    }

    public void deleteById(Long id) {
        Session session = sessionCreator.beginTransaction();
        session.get(Ticket.class, id).
                setDeleted(true);
        session.getTransaction().commit();
    }
}
