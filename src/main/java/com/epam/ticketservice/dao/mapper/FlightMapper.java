package com.epam.ticketservice.dao.mapper;

import com.epam.ticketservice.entities.Flight;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class FlightMapper implements RowMapper<Flight> {

    @Override
    public Flight mapRow(ResultSet resultSet, int i) throws SQLException {
        return Flight.newFlightBuilder().
                setId(resultSet.getLong("ID")).
                setName(resultSet.getString("NAME")).
                setNumberSeats(resultSet.getInt("NUMBER_SEATS")).
                setDateDeparture(resultSet.getDate("DATE_DEPARTURE").toLocalDate()).
                setFlightDuration(resultSet.getInt("FLIGHT_DURATION")).
                setDepartureFrom(resultSet.getString("DEPARTURE_FROM")).
                setDepartureTo(resultSet.getString("DEPARTURE_TO")).
                setIsDeleted(resultSet.getBoolean("IS_DELETED")).
                build();
    }
}
