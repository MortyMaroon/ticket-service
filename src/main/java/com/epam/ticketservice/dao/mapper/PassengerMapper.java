package com.epam.ticketservice.dao.mapper;

import com.epam.ticketservice.entities.Passenger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class PassengerMapper implements RowMapper<Passenger> {

    @Override
    public Passenger mapRow(ResultSet resultSet, int i) throws SQLException {
        return Passenger.newPassengerBuilder().
                setId(resultSet.getLong("ID")).
                setFirstName(resultSet.getString("FIRST_NAME")).
                setLastName(resultSet.getString("LAST_NAME")).
                setPassport(resultSet.getString("PASSPORT")).
                setDeleted(resultSet.getBoolean("IS_DELETED")).
                build();
    }
}
