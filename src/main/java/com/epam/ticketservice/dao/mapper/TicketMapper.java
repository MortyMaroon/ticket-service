package com.epam.ticketservice.dao.mapper;

import com.epam.ticketservice.entities.Ticket;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TicketMapper implements RowMapper<Ticket> {

    @Override
    public Ticket mapRow(ResultSet resultSet, int i) throws SQLException {
        return Ticket.newTicketBuilder().
                setId(resultSet.getLong("ID")).
                setPassengerId(resultSet.getLong("PASSENGER_ID")).
                setFlightId(resultSet.getLong("FLIGHT_ID")).
                setPrice(resultSet.getBigDecimal("PRICE")).
                setDeleted(resultSet.getBoolean("IS_DELETED")).
                build();
    }
}
