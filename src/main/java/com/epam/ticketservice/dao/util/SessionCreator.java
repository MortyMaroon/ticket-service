package com.epam.ticketservice.dao.util;

import com.epam.ticketservice.entities.Flight;
import com.epam.ticketservice.entities.Passenger;
import com.epam.ticketservice.entities.Ticket;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class SessionCreator {
    private SessionFactory sessionFactory;

    @PostConstruct
    private void createSessionFactory() {
        sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Flight.class)
                .addAnnotatedClass(Passenger.class)
                .addAnnotatedClass(Ticket.class)
                .buildSessionFactory();
    }

    public Session beginTransaction() {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        return session;
    }

    public void finishTransaction(Session session){
        session.getTransaction().commit();
    }

    public void shutDownFactory() {
        sessionFactory.close();
    }
}
