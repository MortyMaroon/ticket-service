package com.epam.ticketservice.dto;

import java.time.LocalDate;

public class FlightDTO {
    private Long id;
    private String name;
    private Integer numberSeats;
    private LocalDate dateDeparture;
    private Integer flightDuration;
    private String departureFrom;
    private String departureTo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberSeats() {
        return numberSeats;
    }

    public void setNumberSeats(Integer numberSeats) {
        this.numberSeats = numberSeats;
    }

    public LocalDate getDateDeparture() {
        return dateDeparture;
    }

    public void setDateDeparture(LocalDate dateDeparture) {
        this.dateDeparture = dateDeparture;
    }

    public Integer getFlightDuration() {
        return flightDuration;
    }

    public void setFlightDuration(Integer flightDuration) {
        this.flightDuration = flightDuration;
    }

    public String getDepartureFrom() {
        return departureFrom;
    }

    public void setDepartureFrom(String departureFrom) {
        this.departureFrom = departureFrom;
    }

    public String getDepartureTo() {
        return departureTo;
    }

    public void setDepartureTo(String departureTo) {
        this.departureTo = departureTo;
    }

    public FlightDTO() {
    }

    public static FlightDTO.FlightDTOBuilder newFlightDTOBuilder() {
        return new FlightDTO(). new FlightDTOBuilder();
    }

    public class FlightDTOBuilder {

        public FlightDTOBuilder() {
        }

        public FlightDTOBuilder setId(Long id) {
            FlightDTO.this.id = id;
            return this;
        }

        public FlightDTOBuilder setName(String name) {
            FlightDTO.this.name = name;
            return this;
        }

        public FlightDTOBuilder setNumberSeats(Integer numberSeats) {
            FlightDTO.this.numberSeats = numberSeats;
            return this;
        }

        public FlightDTOBuilder setDateDeparture(LocalDate dateDeparture) {
            FlightDTO.this.dateDeparture = dateDeparture;
            return this;
        }

        public FlightDTOBuilder setFlightDuration(Integer flightDuration) {
            FlightDTO.this.flightDuration = flightDuration;
            return this;
        }

        public FlightDTOBuilder setDepartureFrom(String departureFrom) {
            FlightDTO.this.departureFrom = departureFrom;
            return this;
        }

        public FlightDTOBuilder setDepartureTo(String departureTo) {
            FlightDTO.this.departureTo = departureTo;
            return this;
        }

        public FlightDTO build() {
            return FlightDTO.this;
        }

    }

}
