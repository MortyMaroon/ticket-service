package com.epam.ticketservice.dto;

public class PassengerDTO {
    private Long id;
    private String firstName;
    private String lastName;
    private String passport;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public PassengerDTO() {
    }

    public static PassengerDTOBuilder newPassengerDTOBuilder() {
        return new PassengerDTO(). new PassengerDTOBuilder();
    }

    public class PassengerDTOBuilder {

        public PassengerDTOBuilder() {
        }

        public PassengerDTOBuilder setId(Long id) {
            PassengerDTO.this.id = id;
            return this;
        }

        public PassengerDTOBuilder setFirstName(String firstName) {
            PassengerDTO.this.firstName = firstName;
            return this;
        }

        public PassengerDTOBuilder setLastName(String lastName) {
            PassengerDTO.this.lastName = lastName;
            return this;
        }

        public PassengerDTOBuilder setPassport(String passport) {
            PassengerDTO.this.passport = passport;
            return this;
        }

        public PassengerDTO build() {
            return PassengerDTO.this;
        }

    }

}
