package com.epam.ticketservice.dto;

import java.math.BigDecimal;

public class TicketDTO {
    private Long id;
    private PassengerDTO passenger;
    private FlightDTO flight;
    private BigDecimal price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PassengerDTO getPassenger() {
        return passenger;
    }

    public void setPassenger(PassengerDTO passenger) {
        this.passenger = passenger;
    }

    public FlightDTO getFlight() {
        return flight;
    }

    public void setFlight(FlightDTO flight) {
        this.flight = flight;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public TicketDTO() {
    }

    public static TicketDTO.TicketDTOBuilder newTicketDTOBuilder() {
        return new TicketDTO(). new TicketDTOBuilder();
    }

    public class TicketDTOBuilder {

        public TicketDTOBuilder() {
        }

        public TicketDTO.TicketDTOBuilder setId(Long id) {
            TicketDTO.this.id = id;
            return this;
        }

        public TicketDTO.TicketDTOBuilder setPassenger(PassengerDTO passenger) {
            TicketDTO.this.passenger = passenger;
            return this;
        }

        public TicketDTO.TicketDTOBuilder setFlight(FlightDTO flight) {
            TicketDTO.this.flight = flight;
            return this;
        }

        public TicketDTO.TicketDTOBuilder setPrice(BigDecimal price) {
            TicketDTO.this.price = price;
            return this;
        }

        public TicketDTO build() {
            return TicketDTO.this;
        }

    }

}
