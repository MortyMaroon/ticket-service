package com.epam.ticketservice.entities;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "FLIGHT")
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NUMBER_SEATS")
    private Integer numberSeats;

    @Column(name = "DATE_DEPARTURE")
    private LocalDate dateDeparture;

    @Column(name = "FLIGHT_DURATION")
    private Integer flightDuration;

    @Column(name = "DEPARTURE_FROM")
    private String departureFrom;

    @Column(name = "DEPARTURE_TO")
    private String departureTo;

    @Column(name = "IS_DELETED")
    private Boolean isDeleted;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getNumberSeats() {
        return numberSeats;
    }

    public LocalDate getDateDeparture() {
        return dateDeparture;
    }

    public Integer getFlightDuration() {
        return flightDuration;
    }

    public String getDepartureFrom() {
        return departureFrom;
    }

    public String getDepartureTo() {
        return departureTo;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumberSeats(Integer numberSeats) {
        this.numberSeats = numberSeats;
    }

    public void setDateDeparture(LocalDate dateDeparture) {
        this.dateDeparture = dateDeparture;
    }

    public void setFlightDuration(Integer flightDuration) {
        this.flightDuration = flightDuration;
    }

    public void setDepartureFrom(String departureFrom) {
        this.departureFrom = departureFrom;
    }

    public void setDepartureTo(String departureTo) {
        this.departureTo = departureTo;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Flight() {
    }

    public static FlightBuilder newFlightBuilder() {
        return new Flight().new FlightBuilder();
    }

    public class FlightBuilder {

        public FlightBuilder() {
        }

        public FlightBuilder setId(Long id) {
            Flight.this.id = id;
            return this;
        }

        public FlightBuilder setName(String name) {
            Flight.this.name = name;
            return this;
        }

        public FlightBuilder setNumberSeats(Integer numberSeats) {
            Flight.this.numberSeats = numberSeats;
            return this;
        }

        public FlightBuilder setDateDeparture(LocalDate dateDepart) {
            Flight.this.dateDeparture = dateDepart;
            return this;
        }

        public FlightBuilder setFlightDuration(Integer flightDuration) {
            Flight.this.flightDuration = flightDuration;
            return this;
        }

        public FlightBuilder setDepartureFrom(String departureFrom) {
            Flight.this.departureFrom = departureFrom;
            return this;
        }

        public FlightBuilder setDepartureTo(String departureTo) {
            Flight.this.departureTo = departureTo;
            return this;
        }

        public FlightBuilder setIsDeleted(Boolean isDeleted) {
            Flight.this.isDeleted = isDeleted;
            return this;
        }

        public Flight build() {
            return Flight.this;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Flight)) return false;
        Flight flight = (Flight) o;
        return Objects.equals(id, flight.id) &&
                Objects.equals(name, flight.name) &&
                Objects.equals(numberSeats, flight.numberSeats) &&
                Objects.equals(dateDeparture, flight.dateDeparture) &&
                Objects.equals(flightDuration, flight.flightDuration) &&
                Objects.equals(departureFrom, flight.departureFrom) &&
                Objects.equals(departureTo, flight.departureTo) &&
                Objects.equals(isDeleted, flight.isDeleted);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, numberSeats, dateDeparture, flightDuration, departureFrom, departureTo, isDeleted);
    }
}
