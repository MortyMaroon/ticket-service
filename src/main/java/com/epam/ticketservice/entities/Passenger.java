package com.epam.ticketservice.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "PASSENGER")
public class Passenger {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "PASSPORT")
    private String passport;

    @Column(name = "IS_DELETED")
    private Boolean isDeleted;

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassport() {
        return passport;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Passenger() {
    }

    public static PassengerBuilder newPassengerBuilder() {
        return new Passenger().new PassengerBuilder();
    }

    public class PassengerBuilder {

        public PassengerBuilder() {
        }

        public PassengerBuilder setId(Long id) {
            Passenger.this.id = id;
            return this;
        }

        public PassengerBuilder setFirstName(String firstName) {
            Passenger.this.firstName = firstName;
            return this;
        }

        public PassengerBuilder setLastName(String lastName) {
            Passenger.this.lastName = lastName;
            return this;
        }

        public PassengerBuilder setPassport(String passport) {
            Passenger.this.passport = passport;
            return this;
        }

        public PassengerBuilder setDeleted(Boolean isDeleted) {
            Passenger.this.isDeleted = isDeleted;
            return this;
        }

        public Passenger build() {
            return Passenger.this;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Passenger)) return false;
        Passenger passenger = (Passenger) o;
        return Objects.equals(id, passenger.id) &&
                Objects.equals(firstName, passenger.firstName) &&
                Objects.equals(lastName, passenger.lastName) &&
                Objects.equals(passport, passenger.passport) &&
                Objects.equals(isDeleted, passenger.isDeleted);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, passport, isDeleted);
    }
}
