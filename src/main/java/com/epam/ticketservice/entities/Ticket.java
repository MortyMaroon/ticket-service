package com.epam.ticketservice.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "TICKET")
@NamedQueries({
        @NamedQuery(name = "Ticket.findByPassengerID", query = "select t from Ticket t where t.passengerId = :id"),
        @NamedQuery(name = "Ticket.findByFlightID", query = "select t from Ticket t where t.flightId = :id")
})
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "PASSENGER_ID")
    private Long passengerId;

    @Column(name = "FLIGHT_ID")
    private Long flightId;

    @Column(name = "PRICE")
    private BigDecimal price;

    @Column(name = "IS_DELETED")
    private Boolean isDeleted;

    public Long getId() {
        return id;
    }

    public Long getPassengerId() {
        return passengerId;
    }

    public Long getFlightId() {
        return flightId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPassengerId(Long passengerId) {
        this.passengerId = passengerId;
    }

    public void setFlightId(Long flightId) {
        this.flightId = flightId;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Ticket() {
    }

    public static TicketBuilder newTicketBuilder() {
        return new Ticket().new TicketBuilder();
    }

    public class TicketBuilder {

        public TicketBuilder() {
        }

        public TicketBuilder setId(Long id) {
            Ticket.this.id = id;
            return this;
        }

        public TicketBuilder setPassengerId(Long passengerId) {
            Ticket.this.passengerId = passengerId;
            return this;
        }

        public TicketBuilder setFlightId(Long flightId) {
            Ticket.this.flightId = flightId;
            return this;
        }

        public TicketBuilder setPrice(BigDecimal price) {
            Ticket.this.price = price;
            return this;
        }

        public TicketBuilder setDeleted(Boolean isDeleted) {
            Ticket.this.isDeleted = isDeleted;
            return this;
        }

        public Ticket build() {
            return Ticket.this;
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ticket)) return false;
        Ticket ticket = (Ticket) o;
        return Objects.equals(id, ticket.id) &&
                Objects.equals(passengerId, ticket.passengerId) &&
                Objects.equals(flightId, ticket.flightId) &&
                Objects.equals(price, ticket.price) &&
                Objects.equals(isDeleted, ticket.isDeleted);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, passengerId, flightId, price, isDeleted);
    }
}
