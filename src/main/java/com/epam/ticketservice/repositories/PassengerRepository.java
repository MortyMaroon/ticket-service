package com.epam.ticketservice.repositories;

import com.epam.ticketservice.entities.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PassengerRepository extends JpaRepository<Passenger, Long> {
    @Query("select p from Passenger p where p.isDeleted = false")
    List<Passenger> findAll();
}
