package com.epam.ticketservice.repositories;

import com.epam.ticketservice.entities.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {
    @Query("select t from Ticket t where t.isDeleted = false")
    List<Ticket> findAll();
    List<Ticket> findByPassengerId(Long id);
    List<Ticket> findByFlightId(Long id);
}
