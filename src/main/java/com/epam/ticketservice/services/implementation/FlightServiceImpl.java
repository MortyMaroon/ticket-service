package com.epam.ticketservice.services.implementation;

import com.epam.ticketservice.dto.FlightDTO;
import com.epam.ticketservice.entities.Flight;
import com.epam.ticketservice.repositories.FlightRepository;
import com.epam.ticketservice.services.interfaces.FlightService;
import com.epam.ticketservice.services.interfaces.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightServiceImpl implements FlightService {
    private final FlightRepository flightRepository;
    private TicketService ticketService;

    @Autowired
    public FlightServiceImpl(FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }

    @Autowired
    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @Override
    public List<Flight> findAll() {
        return flightRepository.findAll();
    }

    @Override
    public Flight findById(Long id) {
        return flightRepository.findById(id).get();
    }

    @Override
    public Flight save(FlightDTO flight) {
        Flight newFlight = flightRepository.save(collectEntity(flight));
        ticketService.createTicketDtoByFlightDTO(newFlight);
        return newFlight;
    }

    @Override
    public Flight update(FlightDTO flight) {
        return flightRepository.save(collectEntity(flight));
    }

    @Override
    public void deleteById(Long id) {
        flightRepository.deleteById(id);
        ticketService.deleteTicketBiFlightId(id);
    }

    @Override
    public FlightDTO collectDTO(Flight flight) {
        return FlightDTO.newFlightDTOBuilder().setId(flight.getId()).
                setName(flight.getName()).
                setNumberSeats(flight.getNumberSeats()).
                setDateDeparture(flight.getDateDeparture()).
                setFlightDuration(flight.getFlightDuration()).
                setDepartureFrom(flight.getDepartureFrom()).
                setDepartureTo(flight.getDepartureTo()).
                build();
    }

    @Override
    public Flight collectEntity(FlightDTO flight) {
        return Flight.newFlightBuilder().
                setId(flight.getId()).
                setName(flight.getName()).
                setNumberSeats(flight.getNumberSeats()).
                setDateDeparture(flight.getDateDeparture()).
                setFlightDuration(flight.getFlightDuration()).
                setDepartureFrom(flight.getDepartureFrom()).
                setDepartureTo(flight.getDepartureTo()).
                setIsDeleted(false).
                build();
    }
}
