package com.epam.ticketservice.services.implementation;

import com.epam.ticketservice.dto.PassengerDTO;
import com.epam.ticketservice.entities.Passenger;
import com.epam.ticketservice.repositories.PassengerRepository;
import com.epam.ticketservice.services.interfaces.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PassengerServiceImpl implements PassengerService {
    private final PassengerRepository passengerRepository;

    @Autowired
    public PassengerServiceImpl(PassengerRepository passengerRepository) {
        this.passengerRepository = passengerRepository;
    }

    @Override
    public List<Passenger> findAll() {
        return passengerRepository.findAll();
    }

    @Override
    public Passenger findById(Long id) {
        return passengerRepository.findById(id).get();
    }

    @Override
    public Passenger save(PassengerDTO passenger) {
        return passengerRepository.save(collectEntity(passenger));
    }

    @Override
    public Passenger update(PassengerDTO passenger) {
        return passengerRepository.save(collectEntity(passenger));
    }

    @Override
    public void deleteById(Long id) {
        passengerRepository.deleteById(id);
    }

    @Override
    public PassengerDTO collectDTO(Passenger passenger) {
        return PassengerDTO.newPassengerDTOBuilder().
                setId(passenger.getId()).
                setFirstName(passenger.getFirstName()).
                setLastName(passenger.getLastName()).
                setPassport(passenger.getPassport()).
                build();
    }

    @Override
    public Passenger collectEntity(PassengerDTO passenger) {
        return Passenger.newPassengerBuilder().
                setId(passenger.getId()).
                setFirstName(passenger.getFirstName()).
                setLastName(passenger.getLastName()).
                setPassport(passenger.getPassport()).
                setDeleted(false).
                build();
    }
}
