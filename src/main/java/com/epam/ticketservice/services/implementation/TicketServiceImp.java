package com.epam.ticketservice.services.implementation;

import com.epam.ticketservice.dto.TicketDTO;
import com.epam.ticketservice.entities.Flight;
import com.epam.ticketservice.entities.Ticket;
import com.epam.ticketservice.repositories.TicketRepository;
import com.epam.ticketservice.services.interfaces.FlightService;
import com.epam.ticketservice.services.interfaces.PassengerService;
import com.epam.ticketservice.services.interfaces.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class TicketServiceImp implements TicketService {
    private final TicketRepository ticketRepository;
    private FlightService flightService;
    private PassengerService passengerService;

    @Autowired
    public TicketServiceImp(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Autowired
    public void setFlightService(FlightService flightService) {
        this.flightService = flightService;
    }

    @Autowired
    public void setPassengerService(PassengerService passengerService) {
        this.passengerService = passengerService;
    }

    @Override
    public List<Ticket> findAll() {
        return ticketRepository.findAll();
    }

    @Override
    public List<Ticket> findByPassengerId(Long passengerId) {
        return ticketRepository.findByPassengerId(passengerId);
    }

    @Override
    public List<Ticket> findByFlightId(Long flightId) {
        return ticketRepository.findByFlightId(flightId);
    }

    @Override
    public Ticket findById(Long id) {
        return ticketRepository.findById(id).get();
    }

    @Override
    public Ticket update(TicketDTO ticket) {
        return ticketRepository.save(collectEntity(ticket));
    }

    @Override
    public void deleteById(Long id) {
        ticketRepository.deleteById(id);
    }

    @Override
    public void createTicketDtoByFlightDTO(Flight flight) {
        for (int i = 0; i < flight.getNumberSeats(); i++) {
            ticketRepository.save(Ticket.newTicketBuilder().
                    setPassengerId(null).
                    setFlightId(flight.getId()).
                    setPrice(BigDecimal.valueOf(10000)).
                    setDeleted(false).
                    build());
        }
    }

    @Override
    public void deleteTicketBiFlightId(Long flightId) {
        findByFlightId(flightId).stream().
                map(Ticket::getId).
                forEach(this::deleteById);
    }

    @Override
    public TicketDTO collectDTO(Ticket ticket) {
        return TicketDTO.newTicketDTOBuilder().
                setId(ticket.getId()).
                setPassenger(ticket.getPassengerId() != null ?
                        passengerService.collectDTO(passengerService.findById(ticket.getPassengerId())) : null).
                setFlight(ticket.getFlightId() != null ?
                        flightService.collectDTO(flightService.findById(ticket.getFlightId())) : null).
                setPrice(ticket.getPrice()).
                build();
    }

    @Override
    public Ticket collectEntity(TicketDTO ticket) {
        return Ticket.newTicketBuilder().
                setId(ticket.getId()).
                setPassengerId(ticket.getPassenger() != null ? ticket.getPassenger().getId() : null).
                setFlightId(ticket.getFlight() != null ? ticket.getFlight().getId() : null).
                setPrice(ticket.getPrice()).
                setDeleted(false).
                build();
    }

}
