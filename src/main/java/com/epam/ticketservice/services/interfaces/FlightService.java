package com.epam.ticketservice.services.interfaces;

import com.epam.ticketservice.dto.FlightDTO;
import com.epam.ticketservice.entities.Flight;

import java.util.List;

public interface FlightService {
    List<Flight> findAll();
    Flight findById(Long id);
    Flight save(FlightDTO flight);
    Flight update(FlightDTO flight);
    void deleteById(Long id);
    FlightDTO collectDTO(Flight flight);
    Flight collectEntity(FlightDTO flight);
}
