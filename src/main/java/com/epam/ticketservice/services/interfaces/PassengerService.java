package com.epam.ticketservice.services.interfaces;

import com.epam.ticketservice.dto.PassengerDTO;
import com.epam.ticketservice.entities.Flight;
import com.epam.ticketservice.entities.Passenger;

import java.util.List;

public interface PassengerService {
    List<Passenger> findAll();
    Passenger findById(Long id);
    Passenger save(PassengerDTO passenger);
    Passenger update(PassengerDTO passenger);
    void deleteById(Long id);
    PassengerDTO collectDTO(Passenger passenger);
    Passenger collectEntity(PassengerDTO passenger);
}
