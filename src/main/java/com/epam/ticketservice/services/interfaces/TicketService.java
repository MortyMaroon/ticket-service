package com.epam.ticketservice.services.interfaces;

import com.epam.ticketservice.dto.TicketDTO;
import com.epam.ticketservice.entities.Flight;
import com.epam.ticketservice.entities.Ticket;

import java.util.List;

public interface TicketService {
    List<Ticket> findAll();
    List<Ticket> findByPassengerId(Long passengerId);
    List<Ticket> findByFlightId(Long flightId);
    Ticket findById(Long id);
    Ticket update(TicketDTO passenger);
    void deleteById(Long id);
    void createTicketDtoByFlightDTO(Flight flight);
    void deleteTicketBiFlightId(Long flightID);
    TicketDTO collectDTO(Ticket ticket);
    Ticket collectEntity(TicketDTO ticket);
}
