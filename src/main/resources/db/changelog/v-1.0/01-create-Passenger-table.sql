create table if not exists Passenger
(
    ID         bigserial primary key,
    FIRST_NAME varchar(255),
    LAST_NAME  varchar(255),
    PASSPORT   varchar(255),
    IS_DELETED boolean
)