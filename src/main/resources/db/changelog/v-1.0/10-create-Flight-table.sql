create table if not exists Flight
(
    ID              bigserial primary key,
    NAME            varchar(255),
    NUMBER_SEATS    int,
    DATE_DEPARTURE  timestamp,
    FLIGHT_DURATION int,
    DEPARTURE_FROM  varchar(255),
    DEPARTURE_TO    varchar(255),
    IS_DELETED      boolean
)