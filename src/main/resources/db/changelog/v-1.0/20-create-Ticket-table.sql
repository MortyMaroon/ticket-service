create table if not exists Ticket
(
    ID           bigserial primary key,
    PASSENGER_ID bigint,
    FLIGHT_ID    bigint,
    PRICE        decimal,
    IS_DELETED   boolean
)
-- GO
--
-- alter table Ticket
--     add constraint passenger foreign key (PASSENGER_ID) references Passenger (ID)
-- GO
--
-- alter table Ticket
--     add constraint flight foreign key (FLIGHT_ID) references Flight (ID)