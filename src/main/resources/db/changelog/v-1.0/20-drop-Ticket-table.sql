alter table Ticket
    drop constraint passenger
GO

alter table Ticket
    drop constraint flight
GO

drop table constraint
