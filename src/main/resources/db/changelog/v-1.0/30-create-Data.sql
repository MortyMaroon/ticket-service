insert into Passenger (FIRST_NAME, LAST_NAME, PASSPORT, IS_DELETED)
values ('Денис', 'Тихонов', 'Паспорт', false),
       ('Петр', 'Иванов', 'Паспорт1', false),
       ('Андрей', 'Прокофьев', 'Паспорт2', false),
       ('Илья', 'Петров', 'Паспорт3', false)
GO

insert into Flight (NAME, NUMBER_SEATS, DATE_DEPARTURE, FLIGHT_DURATION, DEPARTURE_FROM, DEPARTURE_TO, IS_DELETED)
values ('Test1', 3, '2021-06-14 20:00', 40, 'LAX', 'SVO', false),
       ('Test3', 3, '2014-06-15 20:00', 40, 'LAX', 'SVO', false),
       ('Test3', 3, '2014-06-16 20:00', 40, 'LAX', 'SVO', false),
       ('Test4', 3, '2014-06-17 20:00', 40, 'LAX', 'SVO', false)
GO

insert into Ticket (PASSENGER_ID, FLIGHT_ID, PRICE, IS_DELETED)
values (1, 1, 450, false),
       (2, 1, 450, false),
       (null, 1, 450, false),
       (1, 2, 450, false),
       (3, 2, 450, false),
       (3, 2, 450, false),
       (1, 3, 450, false),
       (4, 3, 450, false),
       (null, 3, 450, false),
       (1, 4, 450, false),
       (2, 4, 450, false),
       (null, 4, 450, false)